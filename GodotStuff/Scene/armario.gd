extends StaticBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	$Label.percent_visible=0
	$Button.hide()
	pass

func _process(delta):
	pass


func _on_Area2D_body_entered(body):
	$AnimatedSprite.play("ArmarioAbrir")
	$Button.show()

func _on_Area2D2_mouse_entered():
	$Label.percent_visible=1
	pass # replace with function body


func _on_Area2D2_mouse_exited():
	$Label.percent_visible=0
	pass # replace with function body


func _on_Button_pressed():
	get_tree().change_scene("res://Scene/Cena2.tscn")
	pass # replace with function body
