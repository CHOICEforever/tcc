extends Node2D


func _ready():
	get_tree().paused = false
	pass


func _input(event):
	if event.is_action_pressed("pause"):
		get_tree().paused = true
		$CanvasLayer/Pause.show()
	pass


func _on_Button_pressed():
	$CanvasLayer/Pause.hide()
	get_tree().paused = false
	pass # Replace with function body.


func _on_Button2_pressed():
	get_tree().change_scene("res://Scene/Menu.tscn")
	pass # Replace with function body.
