extends Node2D

var chave = false
var alavanca = false

func _on_chave_body_entered(body):
	$porta/chave/ButtonChave.show()
	pass # Replace with function body.


func _on_chave_mouse_entered():
	$porta/chave/Label.show()
	pass # Replace with function body.


func _on_chave_mouse_exited():
	$porta/chave/Label.hide()
	pass # Replace with function body.


func _on_presilha_body_entered(body):
	$porta/presilha/ButtonPresilha.show()
	pass # Replace with function body.


func _on_alavanca_body_entered(body):
	$porta/alavanca/ButtonAlavanca.show()
	pass # Replace with function body.


func _on_ButtonAlavanca_pressed():
	if alavanca == true:
		$porta/porta1.play("porta")
		$porta/alavanca/ButtonAlavanca.queue_free()
	pass # Replace with function body.


func _on_ButtonChave_pressed():
	$porta/chave.queue_free()
	chave = true
	pass # Replace with function body.


func _on_ButtonPresilha_pressed():
	if chave == true:
		alavanca = true
		$porta/presilha.queue_free()
	pass # Replace with function body.


func _on_porta_mouse_entered():
	$porta/porta/Label.show()
	pass # Replace with function body.


func _on_porta_mouse_exited():
	$porta/porta/Label.hide()
	pass # Replace with function body.


func _on_espelho_mouse_entered():
	$espelho/Label.show()
	pass # Replace with function body.


func _on_espelho_mouse_exited():
	$espelho/Label.hide()
	pass # Replace with function body.


func _on_caixas_mouse_entered():
	$caixas/Label.show()
	pass # Replace with function body.


func _on_caixas_mouse_exited():
	$caixas/Label.hide()
	pass # Replace with function body.


func _on_presilha_mouse_entered():
	$porta/presilha/Label.show()
	pass # Replace with function body.


func _on_presilha_mouse_exited():
	$porta/presilha/Label.hide()
	pass # Replace with function body.


func _on_cortinas_mouse_entered():
	$cortinas/Label.show()
	pass # Replace with function body.


func _on_cortinas_mouse_exited():
	$cortinas/Label.hide()
	pass # Replace with function body.
