extends StaticBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	$Label.percent_visible=0
	pass

func _on_Area2D_mouse_entered():
	$Label.percent_visible=1
	pass # replace with function body


func _on_Area2D_mouse_exited():
	$Label.percent_visible=0
	pass # replace with function body
